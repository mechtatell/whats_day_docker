FROM maven:3.8.1-openjdk-17-slim as builder
WORKDIR /app
COPY . /app/
RUN mvn package -Dmaven.test.skip

FROM openjdk:17-alpine as runner
WORKDIR /app
EXPOSE 8080
COPY --from=builder /app/target/*.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]