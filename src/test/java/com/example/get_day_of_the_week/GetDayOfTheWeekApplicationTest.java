package com.example.get_day_of_the_week;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class GetDayOfTheWeekApplicationTest {

    @Test
    void getCurrentDayOfWeek() {
        GetDayOfTheWeekApplication getDayOfTheWeekApplication = new GetDayOfTheWeekApplication();
        Date date = new Date(1664988911183L);
        GetDayOfTheWeekApplication.DayOfWeek currentDayOfWeek = getDayOfTheWeekApplication.getCurrentDayOfWeek(date);
        String dayOfWeek = currentDayOfWeek.getDayOfWeek();
        assertEquals("Wed, 5 Oct 2022", dayOfWeek);
    }
}