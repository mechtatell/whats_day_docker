package com.example.get_day_of_the_week;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@RestController
@SpringBootApplication
public class GetDayOfTheWeekApplication {

    public static void main(String[] args) {
        SpringApplication.run(GetDayOfTheWeekApplication.class, args);
    }

    @GetMapping
    public DayOfWeek getDayOfWeek() {
        Date date = new Date();
        System.out.println("Current date " + date.getTime());
        return getCurrentDayOfWeek(date);
    }

    public DayOfWeek getCurrentDayOfWeek(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        dateFormat.applyPattern("EEE, d MMM yyyy");
        String dayOfWeekString = dateFormat.format(date);
        return new DayOfWeek(dayOfWeekString);
    }

    @Data
    @AllArgsConstructor
    static class DayOfWeek {
        private String dayOfWeek;
    }
}
